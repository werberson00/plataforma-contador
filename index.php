<?php
ini_set('display_errors', true);
ob_start();
require_once __DIR__.'/vendor/autoload.php';

use \Source\Core\Session;
use \CoffeeCode\Router\Router;

$session = new Session;
$router = new Router('http://localhost/plataforma-contador', ':');

$router->namespace('Source\App');
$router->get('/', 'Web:home');

$router->get('/login', 'Auth:login');
$router->post('/login', 'Auth:sendLogin');
$router->get('/logout', 'Auth:logout');

/**
 * USUÀRIOS
 */
$router->get('/usuarios', 'User:all');
$router->get('/usuarios/adicionar', 'User:add');

$router->get('/contabilistas', 'Contabilistas:all');
$router->get('/contabilistas/novo', 'Contabilistas:new');
$router->post('/contabilistas/novo', 'Contabilistas:new');
$router->get('/contabilistas/deletar/{id}', 'Contabilistas:delete');

$router->get('/api/contabilistas', 'Contabilistas:getContabilistas');

$router->namespace('Source\App')->group('ops');
$router->get('/{errcode}', 'Web:error');

$router->dispatch();

if ($router->error()) {
    $router->redirect("/ops/{$router->error()}");
}

ob_end_flush();