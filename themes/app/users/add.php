<?php $v->layout('_theme'); ?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            <!--            Adicionar Contabilista-->
            <!-- <small>Optional description</small> -->
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
            <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

        <div class="row">
            <div class="col-md-6">
                <!-- general form elements -->
                <div class="box box-primary">
                    <div class="box-header with-border">
                        <h3 class="box-title">Novo Usuário</h3>
                    </div>
                    <!-- /.box-header -->

                    <!-- form start -->
                    <form role="form" method="POST"">
                    <div class="box-body">
                        <div class="form-group">
                            <label for="razaoSocial">Razão Social</label>
                            <input type="text" class="form-control" id="razaoSocial" placeholder="Razão Social" name="razaoSocial">
                        </div>
                        <div class="form-group">
                            <label for="nomeFantasia">Nome Fantasia</label>
                            <input type="text" class="form-control" id="nomeFantasia" placeholder="Nome Fantasia" name="nomeFantasia">
                        </div>
                        <div class="form-group">
                            <label for="cpfCnpj">Cpf / Cnpj</label>
                            <input type="text" class="form-control" id="cpfCnpj" placeholder="000.000.000.-00 / 00.000.000/0000-00" name="cnpjCpf">
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="text" class="form-control" id="email" placeholder="E-mail" name="email">
                        </div>
                        <div class="form-group">
                            <label for="telefone">Telefone</label>
                            <input type="text" class="form-control" id="telefone" placeholder="Telefone" name="telefone">
                        </div>
                        <div class="form-group">
                            <label for="senha">Senha</label>
                            <input type="password" class="form-control" id="senha" placeholder="Senha" name="senhaUsuario">
                        </div>

                    </div>
                    <!-- /.box-body -->

                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary" id="cadastrar">Cadastrar</button>
                    </div>
                    </form>
                </div>
                <!-- /.box -->
            </div>
        </div>

    </section>
    <!-- /.content -->
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
<script>
    var btnCadastrar = document.querySelector('#cadastrar');
    var token = "<?= session()->userLogged->token; ?>";

    btnCadastrar.addEventListener('click', function(e) {
        e.preventDefault();
        var dados = {
            nomeFantasia: document.querySelector('[name=nomeFantasia]').value,
            razaoSocial: document.querySelector('[name=razaoSocial]').value,
            cnpjCpf: document.querySelector('[name=cnpjCpf]').value,
            email: document.querySelector('[name=email]').value,
            telefone: document.querySelector('[name=telefone]').value,
            senhaUsuario: document.querySelector('[name=senhaUsuario]').value
        }

        axios.post("<?= url('/contabilistas/novo'); ?>", dados)
            .then(resp => {
                if (resp.data.status == 401) {
                    alert(resp.data.erro)
                    return;
                }
                window.location.href = "<?= url('/contabilistas'); ?>"
                console.log(resp)
            })

    })
</script>