<?php $v->layout('_theme'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Szs Sistemas
        <!-- <small>Optional description</small> -->
        </h1>
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Usuários</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="user-all-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Nome</th>
                        <th>E-mail</th>
                        <th>Ativo</th>
                        <th>Tipo de Usuário</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                    <?php foreach($users as $user): ?>
                        <tr>
                            <td><?= ucfirst(mb_strtolower($user['nome'])); ?></td>
                            <td><?= $user['email']; ?></td>
                            <td><?= ($user['ativo'] == 1 ? 'Ativo' : 'Inativo'); ?></td>
                            <td><?= $user['tipoUsuario']; ?></td>
                            <td>C</td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <!-- /.box-body -->
    </div>
    
    </section>
    <!-- /.content -->
</div>