<?php $v->layout('_theme'); ?>

<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
        Szs Sistemas
        <!-- <small>Optional description</small> -->
        </h1>
        <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
        <li class="active">Here</li>
        </ol>
    </section>

    <!-- Main content -->
    <section class="content container-fluid">

    <div class="box">
        <div class="box-header">
            <h3 class="box-title">Contabilistas</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <table id="user-all-table" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Razão social</th>
                        <th>Nome Fantasia</th>
                        <th>E-mail</th>
                        <th>Telefone</th>
                        <th>Cpf/Cnpj</th>
                        <th>Ações</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($contabilistas as $c): ?>
                    <tr id="contabilista-<?= $c['id']; ?>">
                        <td><?= $c['razaoSocial']; ?></td>
                        <td><?= $c['nomeFantasia']; ?></td>
                        <td><?= $c['email']; ?></td>
                        <td><?= $c['telefone']; ?></td>
                        <td><?= $c['cnpjCpf']; ?></td>
                        <td>
                            <button data="<?= $c['id']; ?>" class="alterar btn btn-primary btn-flat"><i class="fa fa-pencil"></i></button>
                            <button data="<?= $c['id']; ?>" class="excluir btn btn-danger btn-flat"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    <!-- /.box-body -->
    </div>
    
    </section>
    <!-- /.content -->
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/axios/0.20.0/axios.min.js" integrity="sha512-quHCp3WbBNkwLfYUMd+KwBAgpVukJu5MncuQaWXgCrfgcxCJAq/fo+oqrRKOj+UKEmyMCG3tb8RB63W+EmrOBg==" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script>

    function getContabilistas()
    {
        axios.get("<?= url('/api/contabilistas'); ?>")
            .then(resp => {
                console.log(resp.data)
                var table = document.querySelector('table tbody')
                table.innerHTML = ''
            })
    }


    Array.prototype.map.call(document.querySelectorAll('.excluir'), function(item, index) {
        item.addEventListener('click', function(e) {
            var id = this.getAttribute('data')

            Swal.fire({
                title: 'Deseja deletar este contabilsta?',
                text: "Esta ação não pode ser revertida!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Sim, deletar!',
                cancelButtonText: 'Não'
            }).then((result) => {
                if (result.value) {
                    axios.get("<?= url('/contabilistas/deletar'); ?>/"+id)
                    .then(resp => {
                        if (resp.data.status == 1) {
                            Swal.fire(
                                'Deletado!'
                                // 'Your file has been deleted.',
                                // 'success'
                            )
                            document.querySelector('#contabilista-'+id).remove()
                            getContabilistas()
                        }
                    })
                }
            })
        })
    })

    Array.prototype.map.call(document.querySelectorAll('.alterar'), function(item) {
        item.addEventListener('click', function(e) {
            var id = this.getAttribute('data')
            confirm('Deseja excluir o usuário')
            // alert('Alterar usuário', id)
        })
    })


</script>