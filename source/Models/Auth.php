<?php 

namespace Source\Models;

use GuzzleHttp\Exception\RequestException;
use Psr\Http\Message\ResponseInterface;
use \Source\Core\Session;

class Auth 
{
    private $ses;

    public function __construct()
    {
        $this->ses = new Session();
    }


    public function login(string $email, string $senha)
    {
        $cliente = new \GuzzleHttp\Client();
        $response = $cliente->post('http://pdc.szssistemas.com.br/api/v1/login', [
            'json' => [
                'email' => $email,
                'senha' => $senha
            ]
        ]);

        $result = json_decode($response->getBody()->getContents(), true);

        if (!$this->ses->has('userLogged')) {
            $this->ses->set('userLogged', $result);
        }
        return true;
    }

    public function validateToken()
    {
        $true = true;
        $token = session()->userLogged->token ;
        try {
            $client = new \GuzzleHttp\Client();
            $response = $client->postAsync('http://pdc.szssistemas.com.br/api/v1/validar', [
                'headers' => ['Authorization' => "Bearer {$token}", "Accept" => "Application/json"]
            ]);
            $response->then(
                function (ResponseInterface $res) {
                    $true = true;
                },
                function (RequestException $e) {
                    if ($e->getCode() != '200') {
                        $true = false;
                    }
                }
            );
            $response->wait();
            return $true;
        } catch (\Exception $e) {
            return false;
        }

    }
}