<?php

namespace Source\Models;
//use GuzzleHttp\Exception\BadResponseException;

class Contabilistas
{
    public function getContabilistas()
    {
        $token = session()->userLogged->token;
        $client = new \GuzzleHttp\Client();
        $promise = $client->getAsync('http://pdc.szssistemas.com.br/api/v1/contabilistas', [
            'headers' => ['Authorization' => "Bearer {$token}", "Accept" => "Application/json"]
        ]);
        $response = $promise->wait();

        return json_decode($response->getBody()->getContents(), true);
    }

    public function novo($data)
    {
        $token = session()->userLogged->token;
        try {
            $c = new \GuzzleHttp\Client();
            $response = $c->post('http://pdc.szssistemas.com.br/api/v1/contabilistas', [
                'headers' => ['Authorization' => "Bearer {$token}", "Accept" => "Application/json"],
                'json' => $data
            ]);
            $result = $response->getStatusCode();
            return [
                'status' => $result
            ];
        } catch (\Exception $e) {
            $erro = $e->getResponse()->getBody()->getContents();
            $data = [
                'status' => '401',
                'erro' => $erro
            ];
            return $data;
        }

    }

    public function delete($id)
    {
        $token = session()->userLogged->token;
        $client = new \GuzzleHttp\Client();
        $promise = $client->delete('http://pdc.szssistemas.com.br/api/v1/contabilistas/'.$id, [
            'headers' => ['Authorization' => "Bearer {$token}", "Accept" => "Application/json"]
        ]);
        $status = $promise->getStatusCode();
        return true;
    }
}
