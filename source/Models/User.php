<?php

namespace Source\Models;

class User 
{
    public function getUsers($usuario)
    {
        $token = session()->userLogged->token;
        $client = new \GuzzleHttp\Client();
        $promise = $client->getAsync("http://pdc.szssistemas.com.br/api/v1/usuarios?usuario={$usuario}", [
            'headers' => ['Authorization' => "Bearer {$token}", "Accept" => "Application/json"]
        ]);
        $response = $promise->wait();
        
        return json_decode($response->getBody()->getContents(), true);
    }

}