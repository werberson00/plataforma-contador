<?php

namespace Source\Core;

use Source\Core\View;
//use Source\Support\Seo;

class Controller
{
    protected $view;
    protected $seo;

    public function __construct(string $pathToViews = null)
    {
        if (!empty($pathToViews)) {
            $this->view = new View($pathToViews);
        } else {
            $this->view = new View(__DIR__.'/../../themes/app/');
        }
//        session()->destroy('userLogged');exit;
//        $_SESSION['userLogged']->token = 'blabla';exit;
        if (session()->has('userLogged') && !empty(session()->userLogged->token)) {
            $auth = new \Source\Models\Auth();
            if (!$auth->validateToken()) {
                session()->destroy('userLogged');
                return redirect('/login');
            }
        } else {
            return redirect('/login');
        }

    }

}