<?php

namespace Source\App;

use Source\Core\Controller;

class Contabilistas extends Controller
{

    public function __construct()
    {
        $session = new \Source\Core\Session();
        if (!$session->has('userLogged')) {
            return redirect('/login');
        }
        parent::__construct(__DIR__.'/../../themes/app/');
    }

    public function all()
    {
        $c = new \Source\Models\Contabilistas;

        echo $this->view->render('contabilistas/all', [
            'contabilistas' => $c->getContabilistas()
        ]);
    }

    public function new()
    {
        if (request_method() == 'POST') {
            $data = json_decode(file_get_contents("php://input"), true);
            $c = new \Source\Models\Contabilistas();
            $created = $c->novo($data);
            echo json_encode($created);
        } else {
            echo $this->view->render('contabilistas/new', []);
        }

    }

    public function delete($id)
    {
        $c = new \Source\Models\Contabilistas();
        if ($c->delete($id['id'])) {
            echo json_encode([
                'status' => 1
            ]);
        }
    }

    public function getContabilistas()
    {
        $c = new \Source\Models\Contabilistas();
        echo json_encode($c->getContabilistas());
    }


}