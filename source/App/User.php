<?php

namespace Source\App;

use \Source\Core\Controller;

class User extends Controller
{
    public function __construct()
    {
        $session = new \Source\Core\Session();
        if (!$session->has('userLogged')) {
            return redirect('/login');
        }
        parent::__construct();
    }

    public function all()
    {
        $user = session()->userLogged->email;

        $u = new \Source\Models\User;
        $users = $u->getUsers($user); 
        echo $this->view->render('users/all', [
            'users' => $users
        ]);
    }

    public function add()
    {
        echo $this->view->render('users/add', []);
    }
}