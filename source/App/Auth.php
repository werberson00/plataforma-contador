<?php

namespace Source\App;

use \Source\Core\Controller;

class Auth extends Controller 
{
    public function __construct()
    {
        parent::__construct();
    }

    public function login()
    {
        if (session()->has('userLogged')) {
            redirect('/');
        }
        echo $this->view->render('login', []);
    }

    public function sendLogin()
    {
        $email = filter_input(INPUT_POST, 'email', FILTER_SANITIZE_SPECIAL_CHARS);
        $senha = filter_input(INPUT_POST, 'senha', FILTER_SANITIZE_SPECIAL_CHARS);
        
        $auth = new \Source\Models\Auth();
        
        if ($auth->login($email, $senha)) {
            return redirect('/');
        }
        return redirect('/login');
    }

    public function logout()
    {
        session()->destroy('userLogged');
        return redirect('');
    }

}