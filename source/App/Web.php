<?php

namespace Source\App;

use Source\Core\Controller;

class Web extends Controller 
{

    public function __construct()
    {
        $session = new \Source\Core\Session();
        if (!$session->has('userLogged')) {
            return redirect('/login');
        }
        parent::__construct(__DIR__.'/../../themes/app/');
    }

    public function home()
    {
        $usuario = session()->userLogged->email;

        $u = new \Source\Models\User;
        $c = new \Source\Models\Contabilistas();
        $usuarios = $u->getUsers($usuario);
        $contabilistas = $c->getContabilistas();
        
        echo $this->view->render('home', [
            'totalContabilistas' => count($contabilistas),
            'totalUsuarios' => count($usuarios)
        ]);
    }

    public function error(array $data)
    {
        dump($data);
    }
}